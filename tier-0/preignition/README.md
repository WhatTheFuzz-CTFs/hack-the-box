# Preignition

## Information Gathering

```
$ nmap -sV 10.129.75.55
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-07 20:43 EDT
Nmap scan report for 10.129.75.55
Host is up (0.013s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE VERSION
80/tcp open  http    nginx 1.14.2
```

We installed `gobuster` and `dirb/common.txt` to find the page `admin.php`.

```
$ gobuster dir --url 10.129.75.55 --wordlist /usr/share/wordlists/dirb/common.txt
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.129.75.55
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirb/common.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/05/07 21:05:39 Starting gobuster in directory enumeration mode
===============================================================
/admin.php            (Status: 200) [Size: 999]

===============================================================
2022/05/07 21:05:50 Finished
===============================================================
```


## Tasks

1. What is considered to be one of the most essential skills to possess as a Penetration Tester?
    * dir busting
1. What switch do we use for nmap's scan to specify that we want to perform version detection
    * `-sV`
1.  What service type is identified as running on port 80/tcp in our nmap scan?
    * `http`
1. What service name and version of service is running on port 80/tcp in our nmap scan?
    * `nginx 1.14.2`
1. What is a popular directory busting tool we can use to explore hidden web directories and resources?
    * `gobuster`
1. What switch do we use to specify to gobuster we want to perform dir busting specifically?
    * `dir`
1. What page is found during our dir busting activities?
    * `admin.php`
1.  What is the status code reported by gobuster upon finding a successful page?
    * 200
