# Crocodile

## Information Gathering

```sh
$ sudo nmap -sV -sC 10.129.75.134
[sudo] password for sean:
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-08 11:03 EDT
Nmap scan report for 10.129.75.134
Host is up (0.032s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
| -rw-r--r--    1 ftp      ftp            33 Jun 08  2021 allowed.userlist
|_-rw-r--r--    1 ftp      ftp            62 Apr 20  2021 allowed.userlist.passwd
| ftp-syst:
|   STAT:
| FTP server status:
|      Connected to ::ffff:10.10.16.30
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 1
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-title: Smash - Bootstrap Business Template
Service Info: OS: Unix
```

We connected to the machine's `ftp` server anonymously with the command: `ftp anonymous@10.129.75.134`. With the command `ls` we saw two files that we downloaded with `get`:

```sh
$ cat allowed.userlist
aron
pwnmeow
egotisticalsw
admin
$ cat allowed.userlist.passwd
root
Supersecretpassword1
@BaASD&9032123sADS
rKXM59ESxesUFHAd
```

Returning to the `nmap` scan, we found that it was using apache to host a website. We used `gobuster` to enumerate it, searching for html and php files.

```sh
$ gobuster dir --url 10.129.75.134 --wordlist /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt --extensions html,php
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.129.75.134
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Extensions:              html,php
[+] Timeout:                 10s
===============================================================
2022/05/08 11:20:12 Starting gobuster in directory enumeration mode
===============================================================
/index.html           (Status: 200) [Size: 58565]
/login.php            (Status: 200) [Size: 1577]
/assets               (Status: 301) [Size: 315] [--> http://10.129.75.134/assets/]
/css                  (Status: 301) [Size: 312] [--> http://10.129.75.134/css/]
/js                   (Status: 301) [Size: 311] [--> http://10.129.75.134/js/]
/logout.php           (Status: 302) [Size: 0] [--> login.php]
/config.php           (Status: 200) [Size: 0]
```

We found the `login.php` page and navigated to it.

![Sign in.](./resources/sign-in.png)

When we logged in with the creds we found earlier(`admin:c7110277ac44d78b6a9fff2232434d16`), we got the flag `c7110277ac44d78b6a9fff2232434d16`

None of the other username and password combos seemed to work.

![Authenticated.](./resources/authenticated.png)
